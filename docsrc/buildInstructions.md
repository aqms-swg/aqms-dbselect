# dbselect build instructions {#build-dbselect}

## Overview

This is a relatively simple build with all code in a single file for each database type (oracle or postgresql).  A single Makefile handles both database types.  Target database type, custom dependency locations etc. are defined in an machine-specific file that is included at build time.  See the [Environment variables](#Environment-variables) section for details.

For now, all postgresql code is in the file 'dbselect.pgc' and oracle code is in 'dbselect.pc'.  These two files should implement exactly the same functionality with their respective target databases. Eventually we intend to move common code into a 'dbselect.c' file and segregate all database-specific code into separate files. This will simplify the task of keeping the two versions synchronized.

When building for an Oracle target, both Sun Solaris and Linux builds are supported.

PostgreSQL-target builds have only been attempted on Linux at this time. 

## Dependencies

### qlib2

The qlib2 library is required for both database types.  This can be from the standard Earthworm distribution as long as it is compiled with the '-f PIC' (Position Independent Code) flag.

### PostgreSQL builds

Besides the standard c language compile tools, compiling for a PostgreSQL target requires the ecpg pre-compiler.  This is available via yum install on RHEL/Centos/Fedora by installing the 'postgresql-devel' package.  On Ubuntu ecpg can be obtained via apt-get of the 'libexpg-dev' package. 

### Oracle builds

Besides the standard c language compile tools, building for an Oracle target requires both a standard Oracle database install and the Oracle 'Pro-C' pre-compiler.  Both are conditionally available free of charge from Oracle. 

## Environment variables

When a build is started, the first thing that is done is the Makefile attempts to read environment variables from a file pointed to by the environment variable 'AQMS_MAKE_ENV_FILE' which specifies targets, directories and dependency locations for the build on the specific build machine.  Theis will later be defaulted to a global environment variable file for all of the AQMS build but for now build throws an error if AQMS_MAKE_ENV_FILE is not set.  AQMS_MAKE_ENV_FILE must point to any flat-text file specifying a minimal set of environment variables.  An example file 'Makefile.env.example' is included in the repository checkout.  This file will need to be renamed and edited to apply to the machine you are building on and run 'export AQMS_MAKE_ENV_FILE=myname.env' to set the environment before building.

### Variables used

* DBTYPE - must be either 'postgresql' or 'oracle'
* BIN_DIR - directory to install compiled binary (defaults to current directory).
* MAN_DIR - directory to install man pages (may be obsolete)
* QLIB2_LIBDIR - directory where libqlib2.a can be found. 
* QLIB2_INCDIR - directory where qlib2.h can be found.

On Oracle builds, you must also define:

* ORACLE_HOME - home directory of Oracle install
* PROCFLAGS - pre-compile flags for Pro-C pre-compiler

## Build

Once the AQMS_MAKE_ENV_FILE variable is defined and the file pointed to is configured, a simple 'make' command in the checkout directory should build the executable.  The executable will be called 'dbselect' regardless of target database.

If DBTYPE = postgresql, the Makefile will use the ecpg pre-processor on the dbselect.pgc file to create a machine-generated standard c file 'dbselect.c'.  Similarly, if DBTYPE=oracle, the 'proc' pre-processor will generate 'dbselect.c' from the file 'dbselect.pc'

The rest of the build is conventional, compiling 'dbselect.c' to the object file 'dbselect.o' and linking to the executable file 'dbselect'.

## Usage

See [usage notes](manpage.md) page.

