# dbselect {#man-dbselect}

## PAGE LAST UPDATED ON

2019-09-10

## NAME

dbselect  - Parametric Information Query Engine.

## VERSION

2.72 (Oracle) and 2.72p (PostgreSQL)

## DESCRIPTION

dbselect is a query tool to get catalog information from an AQMS database.

### OPTIONS

Run `dbselect -h` to get full list of program options:

```
dbselect version 2.72p -- Parametric Information Query Engine.
 
dbselect [-h] [-c file] [-F format] [-Q] [-B] [-I] [-Z] [-f date -t date | -s interval] [-E list] [-p mechtype]
	 [-l min,max] [-L min,max] [-d min,max] [-S selectflag] [-A list] [-U list] [-e list] [-R list] [-G list]
	 [-n min,max] [-g min,max] [-i min,max] [-r min,max] [-T min,max] [-H min,max] [-D min,max]
	 [-q min,max] [-m min,max] [-M list] [-O min,max] [-C lat,lon,min,max] [-P points]
    where:
	-h		Prints out a summary of the available options.
	-c file		Specifies connection file. Overrides DB_FILE variable.
	-F format	Specifies output format.
				ncedc    --> NCEDC readable text format (Default).
         			summary  --> Summary readable text format.
				detail   --> Detailled readable text format.
				gsummary --> Summary readable text format (with geographical type).
				gdetail  --> Detailled readable text format (with geographical type).
				hypo     --> Hypoinverse format.
				hyposha  --> Hypoinverse format (including shadow cards).
				hyposum  --> Hypoinverse format (Summary cards only).
				uw2      --> University of Washington format (A cards only).
				ehpcsv   --> Earthquake Hazards Program CSV format.
				-----------------------------------------------------------
				fpfit	--> Mechanisms in Fpfit format (Default: FP).
				psmeca  --> Mechanisms in psmeca format (Default: MT).
				psvelo  --> Mechanisms in psvelomeca format (Default: FP).
				cmtstd  --> Mechanisms in CMT Standard format (Default: MT).
				mtucb   --> Mechanisms in UCB format (Default: MT).
	-Q		Displays location information (only with ncedc, summary & detail formats).
	-I		Suppress headers (only with ncedc, summary & detail formats).
	-Z		Displays model depth instead of geoid depth (only with ncedc, summary & detail formats).
	-B		Queries the double difference catalog.
	-f date		From date - ignore data before this date.
	-t date		To date - ignore data from this date on.
			Date can be in formats:
				yyyy/mm/dd/hh:mm:ss.ffff
				yyyy/mm/dd.hh:mm:ss.ffff
				yyyy/mm/dd,hh:mm:ss.ffff
				yyyy.ddd,hh:mm:ss.ffff
				yyyy,ddd,hh:mm:ss.ffff
			You may leave off any trailing 0 components of the time.
	-s interval	Span interval.  Alternate way of specifying end time.
			Interval can be an integer followed immediately by
			S, M, H, d, m or y for seconds, minutes, hours, days, months or years.
	-E evid,...	Comma-delimited list of event identifiers.
	-l min,max	Latitude interval (d[:m[:s]]).
	-L min,max	Longitude interval (d[:m[:s]]) N.B. West is negative.
	-d min,max	Depth interval (in conjunction with -Z option)
	-S selectflag	Specifies event selectflag:
				selected    --> Event.selectflag = 1 (Default).
				unselected  --> Event.selectflag = 0.
				all         --> All events.
	-p mechtype	Specifies Mechanisn type (only with fpfit, psmeca, psvelo, cmtstd & mtucb formats).
				MT --> Moment Tensor.
				FP --> Fault Plane.
	-A auth,...	Comma-delimited list of authors (NC,BK,...).
	-U subsrc,...	Comma-delimited list of subsources (RT1,RT2,Jiggle,...).
	-e type,...	Comma-delimited list of event types (ex,px,eq,lp,nt,qb,sn,st,uk,...).
	-G type,...	Comma-delimited list of geographical types (l,r,t).
	-R review,...	Comma-delimited list of review types:
				A: Automatic solution.
				F: Finalized solution.
				H: Human-reviewed solution.
				I: Intermediate solution.
				C: Canceled solution.
	-n min,max	Number of observations interval.
	-g min,max	Gap interval.
	-i min,max	Distance to nearest station interval.
	-r min,max	RMS interval.
	-T min,max	Time error interval.
	-H min,max	Horizontal error interval.
	-D min,max	Depth error interval.
	-q min,max	Quality interval.
	-m min,max	Magnitude interval.
	-M mtype,...	Comma-delimited list of magnitude types (a,d,dl,h,l,w).
	-O min,max	Number of magnitude observations interval.
	-C lat,lon,min,max	Events contained between min & max kilometers (annulus)
				from location specified by lat & lon.
				Example: -C 37.877,-122.235,0.,10.
	-P Xo,Yo,....,Xn,Yn	Events contained in polygonal area defined by the
				points (Xo,Yo) ... (Xn,Yn).
				Example: -P 30.,-122.,40.,-122.,35.,-140.,30.,-122.

 Environment variable DB_FILE --> Overrides default (connect.properties) database connection file.
```

### CONFIGURATION FILE

dbselect needs a configuration file with database connection parameters. By default the name
of this file is assumed to be connect.properties, by default assumed to be located in /usr/local/bin
all parameters are required.

     DB_USER =  database username
     DB_PASSWORD = database password
     DB_NAME = database name, e.g. archdb
     DB_HOST = hostname[:port] (if not specified, defaults to "localhost")


### ENVIRONMENT

Environment variable DB_FILE --> Overrides default (/usr/local/bin/connect.properties) database connection file.

### DEPENDENCIES

Depends on the AQMS schema.

## BUG REPORTING

Bugs reports can be filed on gitlab.com: https://gitlab.com/aqms-swg/aqms-dbselect/issues

## MORE INFORMATION

(a link to build and installation instructions, which are still to be written, should go here)
