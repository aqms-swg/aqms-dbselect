# dbselect {#dbselect}

dbselect standalone client for Oracle and PostgreSQL.

## Usage

See [usage notes](docsrc/manpage.md) page.

## Build Instructions

See [build instructions](docsrc/buildInstructions.md) page.
