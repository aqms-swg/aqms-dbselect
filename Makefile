########################################################################
ifndef AQMS_MAKE_ENV_FILE
$(error AQMS_MAKE_ENV_FILE is undefined.)
endif
 
include $(AQMS_MAKE_ENV_FILE)

TARGET	= dbselect

OBJECT	= $(TARGET).o

UNAME := $(shell uname)

MANEXT	= 1

CC   = gcc
LIBS    = -lm -L $(QLIB2_LIBDIR) -lqlib2
INCLUDE = -I $(QLIB2_INCDIR) 

ifeq ($(DBTYPE), oracle)
	ifeq ($(UNAME), Linux)
		include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
	endif
	ifeq ($(UNAME), SunOS)
		include $(ORACLE_HOME)/precomp/lib/env_precomp.mk.32
	endif
	CFLAGS += -D ORACLE
	PREEXTENSION = pc
	PREPROC = $(ORACLE_HOME)/bin/proc DEFINE=ORACLE $(PROCFLAGS) iname=$(TARGET) include=$(USERINCLUDE)
endif
ifeq ($(DBTYPE), postgresql)
	INCLUDE += -I $(shell pg_config --includedir)
	LIBS += -L $(shell pg_config --libdir) -lecpg
	CFLAGS += -D POSTGRESQL
	PREEXTENSION = pgc
#	ECPGFLAGS = -r no_indicator
	PREPROC = ecpg $(ECPGFLAGS) $(TARGET).pgc
endif


########################################################################

all:	$(TARGET)

$(TARGET): $(OBJECT)
	$(CC) $(COMP_FLAGS) -o $(TARGET) $(OBJECT) $(LIBS)

$(OBJECT): $(TARGET).c
	$(CC) -g $(CFLAGS) $(INCLUDE) -c $(TARGET).c

$(TARGET).c: $(TARGET).$(PREEXTENSION)
	$(PREPROC) 

install:	install_$(TARGET) 

install_$(TARGET):	$(TARGET)
	cp $(TARGET) $(BINDIR)/$(TARGET)

install_man:	$(TARGET).man 
	cp $(TARGET).man $(MANDIR)/man$(MANEXT)/$(TARGET).$(MANEXT)

tar:	$(TARGET)
	(cd ..; tar cf - $(TARGET)/*.pc $(TARGET)/*.pgc $(TARGET)/*.h \
		$(TARGET)/Makefile \
		$(TARGET)/*.man $(TARGET)/$(TARGET) \
		$(TARGET)/README $(TARGET)/CHANGES ) \
	| gzip > /tmp/$(TARGET).tar.gz

clean:
	-rm *.o dbselect.c

veryclean:	clean
	-rm $(TARGET) $(TARGET).c

